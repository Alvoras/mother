package encoding

import (
	"strconv"
	"strings"
	"time"
)

func EpochDecode(seconds string) (time.Time, error) {
	parsedSecs, err := strconv.ParseInt(strings.TrimSpace(seconds), 10, 64)

	return time.Unix(parsedSecs, 0), err
}

func EpochEncode(data string) (int64, error) {
	date, err := time.Parse(time.RFC3339, data)
	return date.Unix(), err
}
