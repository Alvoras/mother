package encoding

import (
	"html"
)

func HTMLEncode(data string) string {
	return html.EscapeString(data)
}

func HTMLDecode(data string) string {
	return html.UnescapeString(data)
}
