package encoding

import (
	b32 "encoding/base32"
)

func B32Encode(data string) string {
	return b32.StdEncoding.EncodeToString([]byte(data))
}

func B32Decode(data string) (string, error) {
	dec, err := b32.StdEncoding.DecodeString(data)
	return string(dec), err
}
