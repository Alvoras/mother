package encoding

import (
	"net/url"
)

func URLEncode(data string) string {
	return url.PathEscape(data)
}

func URLDecode(data string) (string, error) {
	dec, err := url.PathUnescape(data)
	return dec, err
}
