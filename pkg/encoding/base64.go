package encoding

import (
	b64 "encoding/base64"
)

func B64Encode(data string, isURL bool) string {
	var encFunc func([]byte) string

	if isURL {
		encFunc = b64.StdEncoding.EncodeToString
	} else {
		encFunc = b64.URLEncoding.EncodeToString
	}

	return encFunc([]byte(data))
}

func B64Decode(data string, isURL bool) (string, error) {
	var decFunc func(string) ([]byte, error)

	if isURL {
		decFunc = b64.StdEncoding.DecodeString
	} else {
		decFunc = b64.URLEncoding.DecodeString
	}

	dec, err := decFunc(data)

	return string(dec), err
}
