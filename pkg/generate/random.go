package generate

import (
	"crypto/rand"
	"gitlab.com/Alvoras/mother/pkg/misc"
	"math/big"
	"strings"
)

func RandomString(length int, set string, no string) string {
	var letters = map[string]string{
		"default": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
		"symbols": "!\";#$%&'()*+,-./:;<=>?@[]^_`{|}~",
		"numbers": "123456789",
	}

	var removeSets = map[string]string{
		"ambiguous": "{}[]()/\\'\"`~,:;.<>",
		"similar":   "il1Lo0O",
	}

	letterSet := letters["default"]

	for _, cat := range strings.Split(set, ",") {
		if set, ok := letters[cat]; ok {
			letterSet += set
		}
	}

	for _, cat := range strings.Split(no, ",") {
		if set, ok := removeSets[cat]; ok {
			letterSet = misc.RemoveAny(letterSet, set)
		}
	}

	gen := make([]byte, length)

	for i := range gen {
		bigRng, err := rand.Int(rand.Reader, big.NewInt(int64(len(letterSet))))

		if err != nil {
			return "Failed to access the secure prng"
		}

		rng64 := bigRng.Int64()

		gen[i] = letterSet[rng64]
	}

	return string(gen)
}
