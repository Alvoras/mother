package misc

import (
	"errors"
	"fmt"
	"strings"
	"unicode"
	"unicode/utf8"
)

var (
	trimCutset = " \n\t\r\x0B"
)

func ReverseStr(s string) (string, error) {
	return reversePreservingCombiningCharacters(s)
}

// See https://rosettacode.org/wiki/Reverse_a_string#Go
// reversePreservingCombiningCharacters interprets its argument as UTF-8
// and return an error as soon as a byte does not form valid UTF-8. Return value is UTF-8.
func reversePreservingCombiningCharacters(s string) (string, error) {
	if s == "" {
		return "", nil
	}
	p := []rune(s)
	r := make([]rune, len(p))
	start := len(r)
	for i := 0; i < len(p); {
		if p[i] == utf8.RuneError {
			// If we need to quietly skip invalid UTF-8 :
			// i++
			// continue
			return (string(r[start:])), errors.New(fmt.Sprintf("RuneError : Invalid utf-8 character %v", p[i]))
		}
		j := i + 1
		for j < len(p) && (unicode.Is(unicode.Mn, p[j]) ||
			unicode.Is(unicode.Me, p[j]) || unicode.Is(unicode.Mc, p[j])) {
			j++
		}
		for k := j - 1; k >= i; k-- {
			start--
			r[start] = p[k]
		}
		i = j
	}
	return (string(r[start:])), nil
}

func ToLower(s string) string {
	return strings.ToLower(s)
}

func ToUpper(s string) string {
	return strings.ToUpper(s)
}

func TrimLeft(s string) string {
	return strings.TrimLeft(s, trimCutset)
}

func TrimRight(s string) string {
	return strings.TrimRight(s, trimCutset)
}

func FullTrim(s string) string {
	return strings.Trim(s, trimCutset)
}

// See https://socketloop.com/tutorials/golang-remove-characters-from-string-example
func RemoveAny(s string, rmSet string) string {
	filter := func(r rune) rune {
		if strings.IndexRune(rmSet, r) < 0 {
			return r
		}
		return -1
	}

	return strings.Map(filter, s)
}

func Remove(s string, toRemove string) string {
	return strings.Replace(s, toRemove, "", -1) // -1 mean all occurences
}

func Swap(s string) string {
	var swaped []byte
	bytes := []byte(s)

	for _, c := range bytes {
		runeChar := rune(c)

		if unicode.IsLower(runeChar) {
			swaped = append(swaped, byte(unicode.ToUpper(runeChar)))
		} else {
			swaped = append(swaped, byte(unicode.ToLower(runeChar)))
		}
	}

	return string(swaped)
}
