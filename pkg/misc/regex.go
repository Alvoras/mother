package misc

import (
	"encoding/json"
	"fmt"
	"gitlab.com/Alvoras/mother/internal/utils"
	"io/ioutil"
	"path/filepath"
	"strings"
)

// PrintRegexPattern fetches the specified pattern from the config/regex.json file
func PrintRegexPattern(pattern string) string {
	ret := "Pattern not found"
	var err error
	var patterns map[string]string

	dbPath, _ := filepath.Abs("config/regex.json")
	dbData, err := ioutil.ReadFile(dbPath)

	if err != nil {
		ret = fmt.Sprintf("Failed to read the regex db (%s)", dbPath)
	}

	err = json.Unmarshal(dbData, &patterns)

	if err != nil {
		ret = fmt.Sprintf("Failed to load the regex db (%s)", dbPath)
	}

	switch pattern {
	case "all":
		ret = utils.PrettyMap(patterns)
	case "list":
		ret = strings.Join(utils.GetMapKeys(patterns), "\n")
	default:
		if patterns[pattern] != "" {
			ret = patterns[pattern]
		}
	}

	return ret
}
