package hash

import (
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"fmt"
	"gitlab.com/Alvoras/mother/internal/cli"
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/crypto/md4"
	"golang.org/x/crypto/ripemd160"
	"hash"
	"hash/adler32"
	"hash/crc32"
	"hash/crc64"
	"hash/fnv"
	"reflect"
)

// IsHash returns true if at least one member of the Cli.Flags.Hashing struct is true
func IsHash(args cli.Args) bool {
	var ret = false
	// Get the values of the Cli.Flags.Hashing struct
	vals := reflect.Indirect(reflect.ValueOf(args.Flags.Hashing))

	for i := 0; i < vals.NumField(); i++ {
		// Cast the iterated value to a boolean
		buf := vals.Field(i).Interface().(*bool)

		if *buf == true {
			ret = true
			break
		}
	}

	return ret
}

// ToHash returns the hasher's digest in hex form
func ToHash(h hash.Hash, data string) string {
	h.Write([]byte(data))
	return fmt.Sprintf("%x", h.Sum(nil))
}

// GetHasher returns the correct hasher given the arg passed by command line
func GetHasher(args cli.Args) hash.Hash {
	var h hash.Hash
	switch {
	case *args.Flags.Hashing.MD4:
		h = md4.New()

	case *args.Flags.Hashing.MD5:
		h = md5.New()

	case *args.Flags.Hashing.SHA1:
		h = sha1.New()

	case *args.Flags.Hashing.SHA2v224:
		h = sha256.New224()

	case *args.Flags.Hashing.SHA2v256:
		h = sha256.New()

	case *args.Flags.Hashing.SHA3v384:
		h = sha512.New384()

	case *args.Flags.Hashing.SHA3v512:
		h = sha512.New()

	case *args.Flags.Hashing.SHA3v512_224:
		h = sha512.New512_224()

	case *args.Flags.Hashing.SHA3v512_256:
		h = sha512.New512_256()

	case *args.Flags.Hashing.CRC32IEEE:
		h = crc32.New(crc32.MakeTable(crc32.IEEE))

	case *args.Flags.Hashing.CRC32Castagnoli:
		h = crc32.New(crc32.MakeTable(crc32.Castagnoli))

	case *args.Flags.Hashing.CRC32Koopman:
		h = crc32.New(crc32.MakeTable(crc32.Koopman))

	case *args.Flags.Hashing.CRC64ECMA:
		h = crc64.New(crc64.MakeTable(crc64.ECMA))

	case *args.Flags.Hashing.CRC64ISO:
		h = crc64.New(crc64.MakeTable(crc64.ISO))

	case *args.Flags.Hashing.HMAC_MD5:
		h = hmac.New(md5.New, []byte(args.Key))

	case *args.Flags.Hashing.HMAC_SHA1:
		h = hmac.New(sha1.New, []byte(args.Key))

	case *args.Flags.Hashing.HMAC_SHA256:
		h = hmac.New(sha256.New, []byte(args.Key))

	case *args.Flags.Hashing.HMAC_SHA512:
		h = hmac.New(sha512.New, []byte(args.Key))

	case *args.Flags.Hashing.RIPEMD:
		h = ripemd160.New()

	case *args.Flags.Hashing.Adler32:
		h = adler32.New()

	case *args.Flags.Hashing.FNV32:
		h = fnv.New32()

	case *args.Flags.Hashing.FNV32a:
		h = fnv.New32a()

	case *args.Flags.Hashing.FNV64:
		h = fnv.New64()

	case *args.Flags.Hashing.FNV64a:
		h = fnv.New64a()

	}

	return h
}

func BcryptHash(data []byte, wf int) (string, error) {
	res, err := bcrypt.GenerateFromPassword(data, wf)

	return string(res), err
}

func BcryptEval(data []byte, compareTo []byte) (string, error) {
	if err := bcrypt.CompareHashAndPassword(data, compareTo); err != nil {
		return "false", err
	} else {
		return "true", nil
	}
}

func HandleBcrypt(res string, e error) {
	if e != nil {
		fmt.Println(e)
	} else {
		fmt.Println(res)
	}
}
