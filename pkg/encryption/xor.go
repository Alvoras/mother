package encryption

import "fmt"

func XOR(data []byte, key string, format string) string {
	datalen := len(data)
	keylen := len(key)
	out := make([]byte, datalen)

	for i := 0; i < datalen; i++ {
		out[i] = data[i] ^ key[i%keylen]
	}

	switch format {
	case "ascii":
		return string(out)
	case "hex":
		return fmt.Sprintf("%x", out)
	default:
		return string(out)
	}
}
