package encryption

import ()

func ROT13(data string) string {
	return ROT(data, 13)
}

func ROT(data string, offset int) string {
	if offset == 0 {
		offset = 13
	} else if offset < 0 || offset > 26 {
		return "The offset must be between 0 and 26"
	}
	encrypted := make([]byte, len(data))
	for k, v := range data {

		intv := int(v)

		// To uppercase
		if intv > 96 && intv < 123 {
			intv -= 32
		}

		encrypted[k] = byte(intv + offset)
		if encrypted[k] < 65 || encrypted[k] > 90 {
			encrypted[k] = encrypted[k] - 26
		}
	}

	return string(encrypted)
}
