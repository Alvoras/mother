package conversion

import (
	"encoding/hex"
	"errors"
	"strings"
)

func ToASCII(data []byte, from string) (string, error) {
	trimmed := []byte(strings.Trim(string(data), "\n "))

	switch from {
	case Binary:
		if res, err := BaseXToString(string(trimmed), 2); err != nil {
			return "", err
		} else {
			return strings.Join(res, ""), nil
		}

	case Octal:
		if res, err := BaseXToString(string(trimmed), 8); err != nil {
			return "", err
		} else {
			return strings.Join(res, ""), nil
		}

	case Hex:
		merged := strings.Join(strings.Split(string(trimmed), " "), "")

		decoded, err := hex.DecodeString(merged)

		if err != nil {
			return "", err
		}

		return string(decoded), nil

	case Dec: // Decimal to ascii
		if res, err := BaseXToString(string(trimmed), 10); err != nil {
			return "", err
		} else {
			return strings.Join(res, ""), nil
		}
	}

	return "", errors.New("Invalid input type")
}

func BaseXToString(str string, base int) ([]string, error) {
	return BaseXToCodeFormat(str, base, "%c")
}
