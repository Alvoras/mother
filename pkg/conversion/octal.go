package conversion

import (
	"encoding/hex"
	"errors"
	"strings"
)

func ToOctal(data []byte, from string) (string, error) {
	trimmed := []byte(strings.Trim(string(data), "\n "))

	switch from {
	case Binary:
		if res, err := BaseXToOctal(string(trimmed), 2); err != nil {
			return "", err
		} else {
			return strings.Join(res, " "), nil
		}

	case Dec:
		if res, err := BaseXToOctal(string(trimmed), 10); err != nil {
			return "", err
		} else {
			return strings.Join(res, " "), nil
		}

	case Hex:
		merged := strings.Join(strings.Split(string(trimmed), " "), "")

		decoded, err := hex.DecodeString(merged)

		if err != nil {
			return "", err
		}

		return strings.Join(StringToOctal(string(decoded)), " "), nil

	case ASCII: // ASCII to decimal
		return strings.Join(StringToOctal(string(trimmed)), " "), nil
	}

	return "", errors.New("Invalid input type")
}

func StringToOctal(str string) []string {
	return StringToCodeFormat(str, "%o")
}

func BaseXToOctal(str string, base int) ([]string, error) {
	return BaseXToCodeFormat(str, base, "%o")
}
