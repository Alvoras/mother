package conversion

import (
	"encoding/hex"
	"errors"
	"strings"
)

func ToDecimal(data []byte, from string) (string, error) {
	trimmed := []byte(strings.Trim(string(data), "\n "))

	switch from {
	case Binary:
		if res, err := BaseXToDec(string(trimmed), 2); err != nil {
			return "", err
		} else {
			return strings.Join(res, " "), nil
		}

	case Octal:
		if res, err := BaseXToDec(string(trimmed), 8); err != nil {
			return "", err
		} else {
			return strings.Join(res, " "), nil
		}

	case Hex:
		merged := strings.Join(strings.Split(string(trimmed), " "), "")

		decoded, err := hex.DecodeString(merged)

		if err != nil {
			return "", err
		}

		return strings.Join(StringToDec(string(decoded)), " "), nil

	case ASCII:
		return strings.Join(StringToDec(string(trimmed)), " "), nil
	}

	return "", errors.New("Invalid input type")
}

func StringToDec(str string) []string {
	return StringToCodeFormat(str, "%d")
}

func BaseXToDec(str string, base int) ([]string, error) {
	return BaseXToCodeFormat(str, base, "%d")
}
