package conversion

import (
	"errors"
	"strings"
)

func ToHex(data []byte, from string) (string, error) {
	// var out []string
	//
	trimmed := []byte(strings.Trim(string(data), "\n "))

	switch from {
	case Binary:
		if res, err := BaseXToHex(string(trimmed), 2); err != nil {
			return "", err
		} else {
			return strings.Join(res, " "), nil
		}

	case Dec:
		if res, err := BaseXToHex(string(trimmed), 10); err != nil {
			return "", err
		} else {
			return strings.Join(res, " "), nil
		}

	case Octal:
		if res, err := BaseXToHex(string(trimmed), 8); err != nil {
			return "", err
		} else {
			return strings.Join(res, " "), nil
		}

	case ASCII: // ASCII to hex
		return strings.Join(StringToHex(string(trimmed)), " "), nil
	}

	return "", errors.New("Invalid input type")
}

func StringToHex(str string) []string {
	return StringToCodeFormat(str, "%x")
}

func BaseXToHex(str string, base int) ([]string, error) {
	return BaseXToCodeFormat(str, base, "%x")
}
