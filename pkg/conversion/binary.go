package conversion

import (
	"encoding/hex"
	"errors"
	"strconv"
	"strings"
)

func ToBinary(data []byte, from string) (string, error) {
	var out []string

	trimmed := []byte(strings.Trim(string(data), "\n "))

	switch from {
	case Hex:
		hexBuf := make([]byte, hex.DecodedLen(len(trimmed)))
		_, err := hex.Decode(hexBuf, trimmed)

		if err != nil {
			return "", err
		}

		for _, c := range hexBuf {
			out = append(out, ByteToBin(c))
		}

	case Dec:
		buf, err := strconv.Atoi(string(trimmed))
		if err != nil {
			return "", err
		}

		return strconv.FormatInt(int64(buf), 2), nil

	case Octal:
		buf, err := strconv.ParseInt(string(trimmed), 8, 64)
		if err != nil {
			return "", err
		}

		return strconv.FormatInt(int64(buf), 2), nil

	case ASCII:
		for _, c := range trimmed {
			out = append(out, ByteToBin(c))
		}
		return strings.Join(out, " "), nil
	}

	return "", errors.New("Invalid input type")
}

func ByteToBin(c byte) string {
	return strconv.FormatInt(int64(c), 2)
}
