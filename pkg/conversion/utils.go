package conversion

import (
	"fmt"
	"strconv"
	"strings"
)

var (
	ASCII  = "ascii"
	Dec    = "dec"
	Octal  = "octal"
	Binary = "binary"
	Hex    = "hex"
)

func StringToCodeFormat(str string, cf string) []string {
	var ret []string
	for _, n := range str {
		buf := fmt.Sprintf(cf, n)
		ret = append(ret, buf)
	}

	return ret
}

func BaseXToCodeFormat(str string, base int, cf string) ([]string, error) {
	var ret []string
	split := strings.Split(string(str), " ")

	for _, word := range split {
		intN, err := strconv.ParseInt(word, base, 64)
		if err != nil {
			return ret, err
		}

		buf := fmt.Sprintf(cf, intN)
		ret = append(ret, buf)
	}

	return ret, nil
}
