package utils

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"reflect"
	"strings"
)

func Handle(res interface{}, err error) {
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(res)
	}
}

func GetMapKeys(m map[string]string) []string {
	keys := reflect.ValueOf(m).MapKeys()
	strkeys := make([]string, len(keys))
	for i := 0; i < len(keys); i++ {
		strkeys[i] = keys[i].String()
	}
	return strkeys
}

func PrintJSON(m map[string]string) (string, error) {
	str, err := json.MarshalIndent(m, "", "  ")
	return string(str), err
}

func PrettyMap(m map[string]string) string {
	var ret []string

	for key, val := range m {
		ret = append(ret, fmt.Sprintf("\033[1m%s\033[0m: %s", key, val))
	}

	return strings.Join(ret, "\n")
}

func IsPipe() bool {
	ret := false
	info, _ := os.Stdin.Stat()

	if info.Mode()&os.ModeCharDevice == 0 {
		ret = true
	}

	return ret
}

func ReadPipe() string {
	reader := bufio.NewReader(os.Stdin)
	var output []rune

	for {
		input, _, err := reader.ReadRune()
		if err != nil && err == io.EOF {
			break
		}
		output = append(output, input)
	}

	return string(output)
}

func PrintWarnTrailingN(warnTrailingN bool, l *log.Logger) {
	if warnTrailingN {
		l.Println("Found a trailing '\\n'. Have you specified '-n' to echo ?")
	}
}
