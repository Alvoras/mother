package cli

import (
	"fmt"
	"strings"

	"github.com/pborman/getopt/v2"
)

// Args is a struc holding all the cli args that the user can control
type Args struct {
	Data       string
	CompareTo  string
	Key        string
	Format     string
	From       string
	Set        string
	No         string
	WorkFactor *int
	Length     *int
	Offset     *int
	Regex      string
	Flags      struct {
		Encoding struct {
			Base32    *bool
			Base64    *bool
			Base64URL *bool // https://www.ietf.org/rfc/rfc4648.txt [page 6]
			URL       *bool
			HTML      *bool
			Epoch     *bool
		}
		Hashing struct {
			MD4             *bool
			MD5             *bool
			SHA1            *bool
			SHA2v224        *bool
			SHA2v256        *bool
			SHA3v384        *bool
			SHA3v512        *bool
			SHA3v512_224    *bool
			SHA3v512_256    *bool
			CRC32IEEE       *bool
			CRC32Castagnoli *bool
			CRC32Koopman    *bool
			CRC64ECMA       *bool
			CRC64ISO        *bool
			HMAC_MD5        *bool
			HMAC_SHA1       *bool
			HMAC_SHA256     *bool
			HMAC_SHA512     *bool
			RIPEMD          *bool
			Adler32         *bool
			FNV32           *bool
			FNV32a          *bool
			FNV64           *bool
			FNV64a          *bool
			Bcrypt          *bool
		}
		Encryption struct {
			XOR   *bool
			ROT13 *bool
			ROT   *bool
		}
		Conversion struct {
			Binary  *bool
			Hex     *bool
			ASCII   *bool
			Decimal *bool
			Octal   *bool
		}
		Misc struct {
			StringOperations struct {
				Reverse   *bool
				ToLower   *bool
				ToUpper   *bool
				Swap      *bool
				TrimLeft  *bool
				TrimRight *bool
				FullTrim  *bool
				RemoveAny *bool
				Remove    *bool
			}
		}
		Generate struct {
			RandomString *bool
		}
		Decode  *bool
		Encode  *bool
		Eval    *bool
		Warning *bool
		Verbose *bool
		Help    *bool
	}
	Auto struct {
		Pipe bool
	}
}

var (
	encodingReq = []string{
		"encode",
		"decode",
	}

	keyReq = []string{
		"key",
	}

	cutset = []string{
		"set",
	}

	compare = []string{
		"compare",
	}

	randomString = []string{
		"length",
	}

	requiredArray = map[string][]string{
		"hmac-md5":      keyReq,
		"hmac-sha1":     keyReq,
		"hmac-sha256":   keyReq,
		"hmac-sha512":   keyReq,
		"xor":           keyReq,
		"b32":           encodingReq,
		"b64":           encodingReq,
		"b64url":        encodingReq,
		"url":           encodingReq,
		"epoch":         encodingReq,
		"html":          encodingReq,
		"rmany":         cutset,
		"eval":          compare,
		"random-string": randomString,
	}
)

func CheckRequired(set *getopt.Set) bool {
	missing := true
	requiredOptModeSet := false

	for mode, opts := range requiredArray {
		if set.IsSet(mode) {
			requiredOptModeSet = true
			for _, opt := range opts {
				isset := set.IsSet(opt)
				if isset {
					missing = false
					break
				}
			}
			if missing {
				printRequired(mode, opts)
				break
			}
		}
	}

	if !requiredOptModeSet {
		missing = false
	}

	return missing
}

func printRequired(mode string, missings []string) {
	fmt.Printf("Missing required --%s with --%s (see usage with -h or --help)\n", strings.Join(missings, " or --"), mode)
}
