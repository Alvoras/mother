# Mother
#### The CTF Operation Center

# TODO
+ Encoding
  + [x] Base32
  + [x] Base64
  + [x] Base64URL
  + [x] URL
  + [x] HTML
  + [x] Unix epoch
  + [ ] IP to hex
  + [ ] IP to octal
  + [ ] IP to dword
  + [ ] Gray
+ Hashes
  + [x] MD4
  + [x] MD5
  + [x] SHA-1
  + [x] SHA-2
    + [x] SHA-2-224
    + [x] SHA-2-256
    + [x] SHA-2-384
  + [x] SHA-3
    + [x] SHA-3-384
    + [x] SHA-3-512
    + [x] SHA-3-512/224
    + [x] SHA-3-512/256
  + [x] HMAC-MD5
  + [x] HMAC-SHA1
  + [x] HMAC-SHA256
  + [x] HMAC-SHA512
  + [x] RIPEMD-160
  + [x] CRC32 (IEEE)
  + [x] CRC32 (Castagnoli)
  + [x] CRC32 (Koopman)
  + [x] CRC64 (ISO)
  + [x] CRC64 (ECMA)
  + [x] Adler-32
  + [x] bcrypt
+ Encryption
  + [x] XOR
  + [x] ROT13
  + [x] ROTx
  + [ ] AES GCM
  + [ ] AES XTS
  + [ ] AES ECB
+ Parsing
  + [ ] DNS Exfiltration
  + [ ] Memdump
+ Text
  + [x] Binary
  + [x] Hex
  + [x] ASCII
  + [x] Octal
  + [x] Decimal
+ Misc
  + [x] Regex (PCRE)
    + [x] Mail
    + [x] IPv4
    + [x] IPv6 (see https://stackoverflow.com/a/17871737)
    + [x] Valid credit cards (Mastercard, Visa, American Express, Discover)
    + [x] Mastercard
    + [x] Visa
    + [x] Date (mm-dd-yyy)
    + [x] Date (dd-mm-yyyy)
    + [x] Date (yyyy-mm-dd)
    + [x] Strong password (8 chars, one upper, one lower, one number)
  + [x] String operations
    + [x] Reverse
    + [x] Lowercase
    + [x] Uppercase
    + [x] Left-trim
    + [x] Right-trim
    + [x] Full-trim
    + [x] Remove
    + [x] Swap case
  + [ ] External
    + [ ] Hash-identifier
    + [ ] Go-Change-Case
  + [ ] Generate
    + [x] Random string
    + [ ] Crypto secure phrase
