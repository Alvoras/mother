package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/pborman/getopt/v2"
	"gitlab.com/Alvoras/mother/internal/cli"
	"gitlab.com/Alvoras/mother/internal/utils"
	"gitlab.com/Alvoras/mother/pkg/conversion"
	"gitlab.com/Alvoras/mother/pkg/encoding"
	"gitlab.com/Alvoras/mother/pkg/encryption"
	"gitlab.com/Alvoras/mother/pkg/generate"
	"gitlab.com/Alvoras/mother/pkg/hash"
	"gitlab.com/Alvoras/mother/pkg/misc"
)

var (
	args          cli.Args
	warnTrailingN = false
	// Warning is the logger that handles optional warnings
	Warning *log.Logger
)

func init() {
	tempData := getopt.StringLong("input", 'i', "", "Input data")
	tempCompare := getopt.StringLong("compare", 0, "", "Comparison data")
	tempKey := getopt.StringLong("key", 'k', "", "Key to use with the given algorithm")
	tempFmt := getopt.StringLong("format", 'o', "", "Output format [hex|ascii]")
	tempFrom := getopt.StringLong("from", 't', "", "Type of input [ascii|hex|binary|dec|octal]")
	tempSet := getopt.StringLong("set", 's', "", "Set to use with the given functionality")
	tempNo := getopt.StringLong("no", 0, "", "Specify the option NOT to use with the given functionality ")
	tempRegex := getopt.StringLong("regex", 0, "", "Regex pattern to print")

	args.Flags.Hashing.MD4 = getopt.BoolLong("md4", 0, "MD4 checksum")
	args.Flags.Hashing.MD5 = getopt.BoolLong("md5", 0, "MD5 checksum")
	args.Flags.Hashing.SHA1 = getopt.BoolLong("sha1", 0, "SHA-1 checksum")
	args.Flags.Hashing.SHA2v224 = getopt.BoolLong("sha2-224", 0, "SHA-2-224 checksum")
	args.Flags.Hashing.SHA2v256 = getopt.BoolLong("sha2-256", 0, "SHA-2-256 checksum")
	args.Flags.Hashing.SHA3v384 = getopt.BoolLong("sha3-384", 0, "SHA-3-384 checksum")
	args.Flags.Hashing.SHA3v512 = getopt.BoolLong("sha3-512", 0, "SHA-3-512 checksum")
	args.Flags.Hashing.SHA3v512_224 = getopt.BoolLong("sha3-512-224", 0, "SHA-3-512/224 checksum")
	args.Flags.Hashing.SHA3v512_256 = getopt.BoolLong("sha3-512-256", 0, "SHA-3-512/256 checksum")
	args.Flags.Hashing.CRC32IEEE = getopt.BoolLong("crc32-ieee", 0, "CRC32 checksum using the IEEE 802.3 polynomial")
	args.Flags.Hashing.CRC32Castagnoli = getopt.BoolLong("crc32-castagnoli", 0, "CRC32 checksum using the Castagnoli polynomial")
	args.Flags.Hashing.CRC32Koopman = getopt.BoolLong("crc32-koopman", 0, "CRC32 checksum using the Koopman polynomial")
	args.Flags.Hashing.CRC64ECMA = getopt.BoolLong("crc64-ecma", 0, "CRC64-ECMA checksum")
	args.Flags.Hashing.CRC64ISO = getopt.BoolLong("crc64-iso", 0, "CRC64-ISO checksum")
	args.Flags.Hashing.HMAC_MD5 = getopt.BoolLong("hmac-md5", 0, "MD5 hmac\nRequired :\n\t+ [--key|-k]")
	args.Flags.Hashing.HMAC_SHA1 = getopt.BoolLong("hmac-sha1", 0, "SHA1 hmac\nRequired :\n\t+ [--key|-k]")
	args.Flags.Hashing.HMAC_SHA256 = getopt.BoolLong("hmac-sha256", 0, "SHA256 hmac\nRequired :\n\t+ [--key|-k]")
	args.Flags.Hashing.HMAC_SHA512 = getopt.BoolLong("hmac-sha512", 0, "SHA512 hmac\nRequired :\n\t+ [--key|-k]")
	args.Flags.Hashing.RIPEMD = getopt.BoolLong("ripemd", 0, "RIPEMD-160 hash")
	args.Flags.Hashing.Adler32 = getopt.BoolLong("adler32", 0, "Adler-32 checksum")
	args.Flags.Hashing.FNV32 = getopt.BoolLong("fnv32", 0, "FNV-1 hash")
	args.Flags.Hashing.FNV32a = getopt.BoolLong("fnv32a", 0, "32 bits FNV-1a (xor and multiple operation reversed) hash")
	args.Flags.Hashing.FNV64 = getopt.BoolLong("fnv64", 0, "64 bits FNV-1 hash")
	args.Flags.Hashing.FNV64a = getopt.BoolLong("fnv64a", 0, "64 bits FNV-1a (this variant has the xor and multiple operation reversed) hash")
	args.Flags.Hashing.Bcrypt = getopt.BoolLong("bcrypt", 0, "Generate a bcrypt hash from the given string. Use --factor to specify a work factor (from 4 to 31, default is 10)")

	args.WorkFactor = getopt.IntLong("factor", 0, 10, "Specify the work factor for the given mode")

	args.Flags.Encoding.Base32 = getopt.BoolLong("b32", 0, "Base32 encoding\nRequired :\n\t+ [--encode|-e]\n\t+ [--decode|-d]")
	args.Flags.Encoding.Base64 = getopt.BoolLong("b64", 0, "Base64 encoding\nRequired :\n\t+ [--encode|-e]\n\t+ [--decode|-d]")
	args.Flags.Encoding.Base64URL = getopt.BoolLong("b64url", 0, "The input data is URL decoded before computing the string with base64 encoding\nRequired :\n\t+ [--encode|-e]\n\t+ [--decode|-d]")
	args.Flags.Encoding.URL = getopt.BoolLong("url", 0, "URL encoding\nRequired :\n\t+ [--encode|-e]\n\t+ [--decode|-d]")
	args.Flags.Encoding.HTML = getopt.BoolLong("html", 0, "HTML encoding\nRequired :\n\t+ [--encode|-e]\n\t+ [--decode|-d]")
	args.Flags.Encoding.Epoch = getopt.BoolLong("epoch", 0, "Unix epoch encoding\nRequired :\n\t+ [--encode|-e]\n\t+ [--decode|-d]")

	args.Flags.Decode = getopt.BoolLong("decode", 'd', "Decode mode")
	args.Flags.Encode = getopt.BoolLong("encode", 'e', "Encode mode")
	args.Flags.Eval = getopt.BoolLong("eval", 0, "Eval mode for modes that support comparison")

	args.Flags.Encryption.XOR = getopt.BoolLong("xor", 0, "Xor cipher\nRequired :\n\t+ [--key|-k]")
	args.Flags.Encryption.ROT13 = getopt.BoolLong("rot13", 0, "ROT13 cipher")
	args.Flags.Encryption.ROT = getopt.BoolLong("rot", 0, "ROT cipher, using the given offset")
	args.Offset = getopt.IntLong("offset", 0, 0, "Offset to use with the given mode")

	args.Flags.Conversion.Binary = getopt.BoolLong("binary", 0, "Convert input to binary. Select the right input type with [--format|-t] (ASCII by default)")
	args.Flags.Conversion.Hex = getopt.BoolLong("hex", 0, "Convert input to hexadecimal. Select the right input type with [--format|-t] (ASCII by default)")
	args.Flags.Conversion.ASCII = getopt.BoolLong("ascii", 0, "Convert input to ascii. Select the right input type with [--format|-t] (ASCII by default)")
	args.Flags.Conversion.Decimal = getopt.BoolLong("dec", 0, "Convert input to decimal. Select the right input type with [--format|-t] (ASCII by default)")
	args.Flags.Conversion.Octal = getopt.BoolLong("octal", 0, "Convert input to octal. Select the right input type with [--format|-t] (ASCII by default)")

	args.Flags.Misc.StringOperations.Reverse = getopt.BoolLong("reverse", 0, "Reverse the given string")
	args.Flags.Misc.StringOperations.ToLower = getopt.BoolLong("lower", 0, "Convert the given string to lowercase")
	args.Flags.Misc.StringOperations.ToUpper = getopt.BoolLong("upper", 0, "Convert the given string to uppercase")
	args.Flags.Misc.StringOperations.TrimLeft = getopt.BoolLong("trimleft", 0, "Trim the left part of the string")
	args.Flags.Misc.StringOperations.TrimRight = getopt.BoolLong("trimright", 0, "Trim the right part of the string")
	args.Flags.Misc.StringOperations.FullTrim = getopt.BoolLong("trim", 0, "Trim the whole string")
	args.Flags.Misc.StringOperations.RemoveAny = getopt.BoolLong("rmany", 0, "Remove any occurence from the input string of the given charset with --set or -s")
	args.Flags.Misc.StringOperations.Remove = getopt.BoolLong("rm", 0, "Remove any occurence from the input string of the given string with --set or -s")
	args.Flags.Misc.StringOperations.Swap = getopt.BoolLong("swap", 0, "Swap the case for each letter of the given string")

	args.Flags.Generate.RandomString = getopt.BoolLong("random-string", 0, "Generate a random string with the given --set ([symbols,numbers]) or --no ([ambiguous,similar]) parameters separated by a ','")
	args.Length = getopt.IntLong("length", 'l', 0, "Specify the length for the given mode")

	args.Flags.Warning = getopt.BoolLong("warning", 'w', "Warnings will be displayed")
	args.Flags.Verbose = getopt.BoolLong("verbose", 'v', "Verbose mode")
	args.Flags.Help = getopt.BoolLong("help", 'h', "Display help")

	getopt.Parse()

	if cli.CheckRequired(getopt.CommandLine) {
		os.Exit(1)
	}

	args.Data = *tempData
	args.Key = *tempKey
	args.Format = *tempFmt
	args.From = *tempFrom
	args.Set = *tempSet
	args.No = *tempNo
	args.CompareTo = *tempCompare
	args.Regex = *tempRegex

	if *args.Flags.Help {
		getopt.PrintUsage(os.Stdout)
		return
	}

	if *args.Flags.Warning {
		Warning = log.New(os.Stdout, "[!] ", 0)
	} else {
		Warning = log.New(ioutil.Discard, "[!] ", 0)
	}

	if utils.IsPipe() {
		args.Auto.Pipe = true
		args.Data = utils.ReadPipe()
		if args.Data[len(args.Data)-1] == '\n' {
			warnTrailingN = true
		}
	}
}

func main() {
	switch {
	case *args.Flags.Encoding.Base32:
		switch {
		case *args.Flags.Decode:
			utils.Handle(encoding.B32Decode(args.Data))

		case *args.Flags.Encode:
			fmt.Println(encoding.B32Encode(args.Data))
		}

	case *args.Flags.Encoding.Base64URL:
		switch {
		case *args.Flags.Decode:
			utils.Handle(encoding.B64Decode(args.Data, true))

		case *args.Flags.Encode:
			fmt.Println(encoding.B64Encode(args.Data, true))
		}

	case *args.Flags.Encoding.Base64:
		switch {
		case *args.Flags.Decode:
			utils.Handle(encoding.B64Decode(args.Data, false))

		case *args.Flags.Encode:
			fmt.Println(encoding.B64Encode(args.Data, false))
		}

	case *args.Flags.Encoding.URL:
		switch {
		case *args.Flags.Decode:
			utils.Handle(encoding.URLDecode(args.Data))

		case *args.Flags.Encode:
			fmt.Println(encoding.URLEncode(args.Data))
		}

	case *args.Flags.Encoding.HTML:
		switch {
		case *args.Flags.Decode:
			fmt.Println(encoding.HTMLDecode(args.Data))

		case *args.Flags.Encode:
			fmt.Println(encoding.HTMLEncode(args.Data))
		}

	case *args.Flags.Hashing.Bcrypt:
		switch *args.Flags.Eval {
		case true:
			utils.Handle(hash.BcryptEval([]byte(args.Data), []byte(args.CompareTo)))
		case false:
			utils.Handle(hash.BcryptHash([]byte(args.Data), *args.WorkFactor))
		}

	case hash.IsHash(args):
		h := hash.GetHasher(args)

		utils.PrintWarnTrailingN(warnTrailingN, Warning)
		fmt.Println(hash.ToHash(h, args.Data))

	case *args.Flags.Encoding.Epoch:
		switch {
		case *args.Flags.Decode:
			utils.Handle(encoding.EpochDecode(args.Data))

		case *args.Flags.Encode:
			utils.Handle(encoding.EpochEncode(args.Data))
		}

	case *args.Flags.Encryption.XOR:
		fmt.Println(encryption.XOR([]byte(args.Data), args.Key, args.Format))

	case *args.Flags.Encryption.ROT13:
		fmt.Println(encryption.ROT13(args.Data))

	case *args.Flags.Encryption.ROT:
		fmt.Println(encryption.ROT(args.Data, *args.Offset))

	case *args.Flags.Conversion.Binary:
		utils.Handle(conversion.ToBinary([]byte(args.Data), args.From))

	case *args.Flags.Conversion.Hex:
		utils.Handle(conversion.ToHex([]byte(args.Data), args.From))

	case *args.Flags.Conversion.ASCII:
		utils.Handle(conversion.ToASCII([]byte(args.Data), args.From))

	case *args.Flags.Conversion.Decimal:
		utils.Handle(conversion.ToDecimal([]byte(args.Data), args.From))

	case *args.Flags.Conversion.Octal:
		utils.Handle(conversion.ToOctal([]byte(args.Data), args.From))

	case *args.Flags.Misc.StringOperations.Reverse:
		utils.Handle(misc.ReverseStr(string(args.Data)))

	case *args.Flags.Misc.StringOperations.ToLower:
		fmt.Println(misc.ToLower(string(args.Data)))

	case *args.Flags.Misc.StringOperations.ToUpper:
		fmt.Println(misc.ToUpper(string(args.Data)))

	case *args.Flags.Misc.StringOperations.TrimLeft:
		fmt.Println(misc.TrimLeft(string(args.Data)))

	case *args.Flags.Misc.StringOperations.TrimRight:
		fmt.Println(misc.TrimRight(string(args.Data)))

	case *args.Flags.Misc.StringOperations.FullTrim:
		fmt.Println(misc.FullTrim(string(args.Data)))

	case *args.Flags.Misc.StringOperations.RemoveAny:
		fmt.Println(misc.RemoveAny(string(args.Data), args.Set))

	case *args.Flags.Misc.StringOperations.Remove:
		fmt.Println(misc.Remove(string(args.Data), args.Set))

	case *args.Flags.Misc.StringOperations.Swap:
		fmt.Println(misc.Swap(args.Data))

	case *args.Flags.Generate.RandomString:
		fmt.Println(generate.RandomString(*args.Length, args.Set, args.No))

	case args.Regex != "":
		fmt.Println(misc.PrintRegexPattern(args.Regex))
	}
}
